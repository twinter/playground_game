# Simple example in p5.js

This project uses [p5.js](https://p5js.org/).

All available functions of it can be found here (with explanations): <https://p5js.org/reference/>

## Running the project

- open index.html in you browser or through the equivalent option of your IDE

## Troubleshooting

### No type hints or autocompletion in Jetbrains IDE

See <https://stackoverflow.com/a/58753111>.
