let diameter = 25
let y = 50
let dy = 1
let ddy = 1

// The statements in the setup() function
// execute once when the program begins
function setup() {
    createCanvas(720, 400); // Size must be the first statement
    stroke(255); // Set line drawing color to white
    frameRate(60);
}

// The statements in draw() are executed until the
// program is stopped. Each statement is executed in
// sequence and after the last line is read, the first
// line is executed again.
function draw() {
    background(0); // Set the background to black
    y += dy
    dy += ddy
    if (y + diameter / 2 >= height) {
        dy *= -1
    } else if (y - diameter / 2 <= 0) {
        dy *= -1
    }

    circle(50, y, diameter);
}